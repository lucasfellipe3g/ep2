package View;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class Creditos extends JDialog {

        static JPanel jp1,jp2;
        static JLabel l1,l2,l3,l4;
        static JButton b1;
        private Container janela;
        public Creditos()
        {
            setTitle("Créditos do Autor");
            //setBounds(150,150,450,400);

            jp1 = new JPanel();
            jp1.setLayout(new GridLayout(6,1,8,8));
            jp2 = new JPanel();
            jp2.setLayout(new FlowLayout());
            l1 = new JLabel("Universidade de Brasília - UnB");
            l2 = new JLabel("Cursando Engenharia de Software");
            l3 = new JLabel("Aluno: Lucas Fellipe Carvalho Moreira");
            l4 = new JLabel("E-mail: lucasfcm9@gmail.com");
            b1 = new JButton("OK");
            //b1.addActionListener(this);
            jp1.add(l1);
            jp1.add(l2);
            jp1.add(l3);
            jp1.add(l4);
            jp2.add(b1);
            janela = getContentPane();
            janela.add(jp1,BorderLayout.NORTH);
            janela.add(jp2,BorderLayout.SOUTH);
            pack();
            b1.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt)
                {
                    setVisible(false);
                }
            });
        }
	public void actionPerformed(ActionEvent evt)
	{
		Object source = evt.getSource();
		if(source == b1)
		{
			dispose();
			InterfaceTrainerPokemon window = new InterfaceTrainerPokemon();
			window.setVisible(true);
		}
	}
    }
