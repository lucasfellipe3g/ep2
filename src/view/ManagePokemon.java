package View;

import Model.Pokemon;

import javax.swing.*;
import java.util.ArrayList;

public class ManagePokemon {
	

    ArrayList<Pokemon> Pokemons = new ArrayList<>();

    public static boolean searchPokemonByName(ArrayList<Pokemon> pokemon, String searchName) {

        int i = 0;
        boolean found = false;
        while (found == false && i < pokemon.size()) {
            if (pokemon.get(i).getNome().compareToIgnoreCase(searchName) == 0) {
                found = true;
            } else {
                i++;
            }
        }

        if (found) {
            JOptionPane.showMessageDialog(null, "\nNome: " + pokemon.get(i).getNome() +
                    "\nTipo 1: " + pokemon.get(i).getType1() + "\nTipo 2: " + pokemon.get(i).getType2() +
                    "\nTotal: " + pokemon.get(i).getTotal() + "\nHP: " + pokemon.get(i).getHp() + "\nAtaque: " +
                    pokemon.get(i).getAttack() + "\nDefesa: " + pokemon.get(i).getDefense() + "\nSp.Atk: " + pokemon.get(i).getSpecialAttack() +
                    "\nSp.Def: " + pokemon.get(i).getSpecialDefense() + "\nVelocidade: " + pokemon.get(i).getSpeed() + "\nGeração: " + pokemon.get(i).getGeneration() +
                    "\nLendário: " + pokemon.get(i).getLegendary() + "\nExperience: " + pokemon.get(i).getExperience() + "\nHeight: " + pokemon.get(i).getHeight() +
                    "\nWeight: " + pokemon.get(i).getWeight() + "\nHabilidade 1: " + pokemon.get(i).getAbilitie1() + "\nHabilidade 2: " + pokemon.get(i).getAbilitie2()
                    + "\nHabilidade 3: " + pokemon.get(i).getAbilitie3() + "\nMove 1: " + pokemon.get(i).getMove1() + "\nMove 2: " + pokemon.get(i).getMove2() +
                    "\nMove 3: " + pokemon.get(i).getMove3() + "\nMove 4: " + pokemon.get(i).getMove4() + "\nMove 5: " + pokemon.get(i).getMove5()
                    + "\nMove 6: " + pokemon.get(i).getMove6() + "\nMove 7: " + pokemon.get(i).getMove7());
            return false;
        } else {
            JOptionPane.showMessageDialog(null, "Name not found!");
            return true;
        }
    }

    public static boolean searchPokemonByType(ArrayList<Pokemon> pokemon, String searchType) {

        boolean found = false;
        int i = 0;
        while(found == false && i < pokemon.size()) {
            if(pokemon.get(i).getType1().compareToIgnoreCase(searchType) == 0) {
                found = true;
            }

            else {
                i++;
            }
        }

        if(found) {
            for(int j = 0; j < pokemon.size(); j++) {
                if(pokemon.get(j).getType1().compareToIgnoreCase(searchType) == 0) {
                    JOptionPane.showMessageDialog(null, "Nome: " + pokemon.get(j).getNome() + "\nTipo 1: " + pokemon.get(j).getType1() +
                            "\nTipo 2: " + pokemon.get(j).getType2());
                }
            }
            return false;
        }
        else {
            JOptionPane.showMessageDialog(null, "Type not found!");
            return true;
        }
    }
}


