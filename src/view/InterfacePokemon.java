package View;

import Model.Pokemon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class InterfacePokemon extends JDialog {

    static JPanel J1, J2;
    static JLabel l1, l2, l3, l4, l5, l6, l7, l8, l9;
    static JButton button;
    private Container janela;

    public InterfacePokemon() {

        setTitle("Pokémon Data");
        setBounds(150, 150, 450, 430);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
            }
        });

        J1 = new JPanel();
        J1.setLayout(new GridLayout(11, 1, 10, 10));
        J2 = new JPanel();
        J2.setLayout(new FlowLayout());

        l1 = new JLabel("");
        l2 = new JLabel("");
        l3 = new JLabel("");
        l4 = new JLabel("");
        l5 = new JLabel("");
        l6 = new JLabel("");
        l7 = new JLabel("");
        l8 = new JLabel("");
        l9 = new JLabel("");
        button = new JButton("OK");

        J1.add(l1);
        J1.add(l2);
        J1.add(l3);
        J1.add(l4);
        J1.add(l5);
        J1.add(l6);
        J1.add(l7);
        J1.add(l8);
        J1.add(l9);

        janela = getContentPane();
        janela.add(J1, BorderLayout.NORTH);
        janela.add(J2, BorderLayout.SOUTH);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                setVisible(false);
            }
        });
    }

    public static void showPokemon(Pokemon p) {

        l1.setText("Nome: " + p.getNome());
        l2.setText("Tipo 1: " + p.getType1());
        l3.setText("Tipo 2: " + p.getType2());
        l4.setText("Total: " + p.getTotal());
        l5.setText("HP: " + p.getHp());
        l6.setText("Ataque: " + p.getAttack());
        l7.setText("Defesa: " + p.getDefense());
        l8.setText("Ataque Especial: " + p.getSpecialAttack());
        l9.setText("Defesa: " + p.getSpecialDefense());
    }
}
