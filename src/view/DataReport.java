package View;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class DataReport extends JDialog {

    static JPanel jp1,jp2;
    static JLabel l1,l2,l3,l4,l5,l6,l7,l8;
    static JButton b1;
    private Container janela;

    public DataReport() {

        setTitle("Relatório dos Treinadores");
        setBounds(150, 150, 450, 430);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        jp1 = new JPanel();
        jp1.setLayout(new GridLayout(11, 1, 10, 10));
        jp2 = new JPanel();
        jp2.setLayout(new FlowLayout());
        l1 = new JLabel("");
        l2 = new JLabel("");
        l3 = new JLabel("");
        l4 = new JLabel("");
        l5 = new JLabel("");
        l6 = new JLabel("");
        l7 = new JLabel("");
        l8 = new JLabel("");
        b1 = new JButton("OK");
        //b1.addActionListener(this);
        jp1.add(l1);
        jp2.add(b1);
        jp1.add(l2);
        jp1.add(l3);
        jp1.add(l4);
        jp1.add(l5);
        jp1.add(l6);
        jp1.add(l7);
        jp1.add(l8);

        janela = getContentPane();
        janela.add(jp1, BorderLayout.NORTH);
        janela.add(jp2, BorderLayout.SOUTH);

        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                setVisible(false);
            }
        });
    }

    public void showTrainer(Trainer t) {
        l1.setText("Nome: " + t.getNome());
        l2.setText("Login: " + t.getLogin());
        l3.setText("Senha: " + t.getSenha());
        l4.setText("Pokemon 1: " + t.getNomePokemon1());
        l5.setText("Pokemon 2: " + t.getNomePokemon2());
        l6.setText("Pokemon 3: " + t.getNomePokemon3());
        l7.setText("Pokemon 4: " + t.getNomePokemon4());
        l8.setText("Pokemon 5: " + t.getNomePokemon5());
    }
}
