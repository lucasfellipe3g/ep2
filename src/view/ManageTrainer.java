package View;
import javax.swing.JOptionPane;

public class ManageTrainer {

    private Trainer trainers[];
    private int numeroTrainers;

    public ManageTrainer() {
        trainers = new Trainer[2];
        numeroTrainers = 0;
    }
    public void inserirTrainer(Trainer trainer) {

        if (numeroTrainers < 2) {
            trainers[numeroTrainers++] = trainer;
            JOptionPane.showMessageDialog(null, "Treinador cadastrado com sucesso!");
        } else {
            JOptionPane.showMessageDialog(null, "Lista de treinadores cheia!");
        }
    }

    public Trainer getTrainer(int i) {
        if(i >= 0 && i < numeroTrainers) {
            return trainers[i];
        }
        else
            JOptionPane.showMessageDialog(null,"Treinador inexistente","Erro", JOptionPane.ERROR_MESSAGE);

        return null;
    }

    public Trainer buscarTrainer(String nome) {
        int a;
        if(numeroTrainers == 0)
            JOptionPane.showMessageDialog(null,"Nenhum treinador foi inserido!");
        else {
            for(a = 0; a < numeroTrainers; a++) {
                if(nome.equalsIgnoreCase(trainers[a].getNome()))
                    return trainers[a];
            }
        }
        return null;
    }

    public void removerTrainer(int i) {

        int a;
        if(i >=0 && i < numeroTrainers)
        {
            for(a = i; a < numeroTrainers; a++)
            {
                trainers[a] = trainers[a + 1];
            }
            trainers[numeroTrainers--] = null;
            JOptionPane.showMessageDialog(null,"Treinador removido com Sucesso!");

        }
        else
            JOptionPane.showMessageDialog(null,"Impossível remover: índice inválido!");

    }
    public int quantTreinadores() {
        return numeroTrainers;
    }
}