package Model;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadCSV {

    public static void reader(ArrayList<Pokemon> pokemons) throws IOException {

        //Quando forem avaliar o projeto, coloquem o caminho do arquivo de acordo com o seu computador!
        BufferedReader reader = new BufferedReader(new FileReader("/Users/lucasfellipe/Desktop/ep2/data/csv_files/POKEMONS_DATA_1.csv"));

        String linha = null;
        Scanner scanner = null;
        int indice = 0;

        while((linha = reader.readLine()) != null) { //Lendo o arquivo linha por linha;
            Pokemon pokemon = new Pokemon();
            scanner = new Scanner(linha);
            scanner.useDelimiter(",");
            while(scanner.hasNext()) {
                String data = scanner.next();
                if(indice == 0)
                    pokemon.setId(Integer.parseInt(data));
                else if(indice == 1)
                    pokemon.setNome(data);
                else if(indice == 2)
                    pokemon.setType1(data);
                else if(indice == 3)
                    pokemon.setType2(data);
                else if(indice == 4)
                    pokemon.setTotal(Integer.parseInt(data));
                else if(indice == 5)
                    pokemon.setHp(Integer.parseInt(data));
                else if(indice == 6)
                    pokemon.setAttack(Integer.parseInt(data));
                else if(indice == 7)
                    pokemon.setDefense(Integer.parseInt(data));
                else if(indice == 8)
                    pokemon.setSpecialAttack(Integer.parseInt(data));
                else if(indice == 9)
                    pokemon.setSpecialDefense(Integer.parseInt(data));
                else if(indice == 10)
                    pokemon.setSpeed(Integer.parseInt(data));
                else if(indice == 11)
                    pokemon.setGeneration(Integer.parseInt(data));
                else if(indice == 12)
                    pokemon.setLegendary(data);
                else if(indice == 13)
                    pokemon.setExperience(Integer.parseInt(data));
                else if(indice == 14)
                    pokemon.setHeight(Integer.parseInt(data));
                else if(indice == 15)
                    pokemon.setWeight(Integer.parseInt(data));
                else if(indice == 16)
                    pokemon.setAbilitie1(data);
                else if(indice == 17)
                    pokemon.setAbilitie2(data);
                else if(indice == 18)
                    pokemon.setAbilitie3(data);
                else if(indice == 19)
                    pokemon.setMove1(data);
                else if(indice == 20)
                    pokemon.setMove2(data);
                else if(indice == 21)
                    pokemon.setMove3(data);
                else if(indice == 22)
                    pokemon.setMove4(data);
                else if(indice == 23)
                    pokemon.setMove5(data);
                else if(indice == 24)
                    pokemon.setMove6(data);
                else if(indice == 25)
                    pokemon.setMove7(data);
                else if(indice == 26)
                    pokemon.setMove7(data);

                else
                    System.out.println("Dado " + data + " inválido");

                indice++;
            }
            indice = 0;
            pokemons.add(pokemon);
        }


        reader.close();
    }
}
