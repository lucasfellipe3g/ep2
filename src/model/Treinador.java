package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Treinador {
    private String nome;
    private String login;
    private String senha;
    private String nomePokemon1;
    private String nomePokemon2;
    private String nomePokemon3;
    private String nomePokemon4;
    private String nomePokemon5;

    public Treinador(String nome, String login, String senha, String nomePokemon1, String nomePokemon2, String nomePokemon3, String nomePokemon4, String nomePokemon5) {
        this.nome = nome;
        this.login = login;
        this.senha = senha;
        this.nomePokemon1 = nomePokemon1;
        this.nomePokemon2 = nomePokemon2;
        this.nomePokemon3 = nomePokemon3;
        this.nomePokemon4 = nomePokemon4;
        this.nomePokemon5 = nomePokemon5;
    }

    public Treinador() {
        this.nome = nome;
        this.login = login;
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNomePokemon1() {
        return nomePokemon1;
    }

    public void setNomePokemon1(String nomePokemon1) {
        this.nomePokemon1 = nomePokemon1;
    }

    public String getNomePokemon2() {
        return nomePokemon2;
    }

    public void setNomePokemon2(String nomePokemon2) {
        this.nomePokemon2 = nomePokemon2;
    }

    public String getNomePokemon3() {
        return nomePokemon3;
    }

    public void setNomePokemon3(String nomePokemon3) {
        this.nomePokemon3 = nomePokemon3;
    }

    public String getNomePokemon4() {
        return nomePokemon4;
    }

    public void setNomePokemon4(String nomePokemon4) {
        this.nomePokemon4 = nomePokemon4;
    }

    public String getNomePokemon5() {
        return nomePokemon5;
    }

    public void setNomePokemon5(String nomePokemon5) {
        this.nomePokemon5 = nomePokemon5;
    }
}
