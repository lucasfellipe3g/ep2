package Controller;

import Model.Pokemon;
import Model.ReadCSV;
import Model.Treinador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class CadastrarTreinador extends Pokemon {

    List<Treinador> treinador = new ArrayList<>();
    ArrayList<Pokemon> pokemons = new ArrayList<>();


    public void cadastrarTreinador() throws IOException {

        ReadCSV.reader(pokemons);
        Scanner scan = new Scanner(System.in);
        boolean cadastrando = true;
        Treinador t = new Treinador();
        Pokemon p = new Pokemon();
        String cadastro;

        while (cadastrando) {
            System.out.println("Bem-vindo ao sistema de cadastro de treinadores Pokemons: ");

            System.out.println("Digite seu nome: ");
            t.setNome(scan.nextLine());

            System.out.println("Digite seu login: ");
            t.setLogin(scan.nextLine());

            System.out.println("Digite a senha: ");
            t.setSenha(scan.nextLine());

            System.out.println("Digite o nome do Pokemon 1: ");
            t.setNomePokemon1(scan.nextLine());

            System.out.println("Digite o nome do Pokemon 2: ");
            t.setNomePokemon2(scan.nextLine());

            System.out.println("Digite o nome do Pokemon 3: ");
            t.setNomePokemon3(scan.nextLine());

            System.out.println("Digite o nome do Pokemon 4: ");
            t.setNomePokemon4(scan.nextLine());

            System.out.println("Digite o nome do Pokemon 5: ");
            t.setNomePokemon5(scan.nextLine());

            System.out.println("Adicionar cadastro a sua lista de treinadores: (S/N)");
            cadastro = scan.nextLine();
            if (cadastro.equalsIgnoreCase("S")) {
                System.out.println("Cadastro adicionado!");
                treinador.add(t);
                t = new Treinador();
            } else if (cadastro.equalsIgnoreCase("N")) {
                System.out.println("Cadastro não efetuado!");
            } else {
                System.out.println("Digite corretamente!");
            }
            String continuar;
            System.out.println("Deseja cadastrar mais treinadores: (S/N)");
            continuar = scan.nextLine();

            if (continuar.equalsIgnoreCase("N")) {
                cadastrando = false;
            } else if (continuar.equalsIgnoreCase("S")) {

            } else {
                System.out.println("Digite corretamente!");
                cadastrando = false;
            }
        }
    }

    public void removerTreinador() {

        boolean remover = false;
        Scanner scan = new Scanner(System.in);
        listarTreinadores();

        String texto;
        System.out.println("Digite o nome do treinador que quer remover: ");
        texto = scan.nextLine();

        for (int i = 0; i < treinador.size(); i++) {
            if (treinador.get(i).getNome().equalsIgnoreCase(texto)) {
                treinador.remove(i);
            }
        }
        System.out.println("NOVA LISTA: ");
        listarTreinadores();

    }

    public void listarTreinadores() {

        if (treinador.size() == 0) {
            System.out.println("Lista de treinadores vazia!");
        } else {
            for (int i = 0; i < treinador.size(); ++i) {
                System.out.println("Nome: " + treinador.get(i).getNome());
                System.out.println("Login: " + treinador.get(i).getLogin());
                System.out.println("Senha: " + treinador.get(i).getSenha());
                System.out.println("Pokemon 1: " + treinador.get(i).getNomePokemon1());
                System.out.println("Pokemon 2: " + treinador.get(i).getNomePokemon2());
                System.out.println("Pokemon 3: " + treinador.get(i).getNomePokemon3());
                System.out.println("Pokemon 4: " + treinador.get(i).getNomePokemon4());
                System.out.println("Pokemon 5: " + treinador.get(i).getNomePokemon5());
                System.out.println("--------------------");
            }
        }
    }
}
