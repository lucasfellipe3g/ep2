package Controller;
import Model.*;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class GerenciandoPokemons extends Pokemon {

    ArrayList<Pokemon> listaPokemons = new ArrayList<>();

    void listarPokemons() throws IOException {

        ReadCSV.reader(listaPokemons);

        for(int i = 0; i < listaPokemons.size(); i++) {
            System.out.println("ID: " + listaPokemons.get(i).getId());
            System.out.println("Nome: " + listaPokemons.get(i).getNome());
            System.out.println("Tipo 1: " + listaPokemons.get(i).getType1());
            System.out.println("Tipo 2: " + listaPokemons.get(i).getType2());
            System.out.println("Total: " + listaPokemons.get(i).getTotal());
            System.out.println("HP: " + listaPokemons.get(i).getHp());
            System.out.println("Ataque: " + listaPokemons.get(i).getAttack());
            System.out.println("Defesa: " + listaPokemons.get(i).getDefense());
            System.out.println("Ataque especial: " + listaPokemons.get(i).getSpecialAttack());
            System.out.println("Defesa especial: " + listaPokemons.get(i).getSpecialDefense());
            System.out.println("Velocidade: " + listaPokemons.get(i).getSpeed());
            System.out.println("Geração: " + listaPokemons.get(i).getGeneration());
            System.out.println("Lendário: " + listaPokemons.get(i).getLegendary());
            System.out.println("Experience: " + listaPokemons.get(i).getExperience());
            System.out.println("Height: " + listaPokemons.get(i).getHeight());
            System.out.println("Weight: " + listaPokemons.get(i).getWeight());
            System.out.println("Habilidade 1: " + listaPokemons.get(i).getAbilitie1());
            System.out.println("Habilidade 2: " + listaPokemons.get(i).getAbilitie2());
            System.out.println("Habilidade 3: " + listaPokemons.get(i).getAbilitie3());
            System.out.println("Move 1: " + listaPokemons.get(i).getMove1());
            System.out.println("Move 2: " + listaPokemons.get(i).getMove2());
            System.out.println("Move 3: " + listaPokemons.get(i).getMove3());
            System.out.println("Move 4: " + listaPokemons.get(i).getMove4());
            System.out.println("Move 5: " + listaPokemons.get(i).getMove5());
            System.out.println("Move 6: " + listaPokemons.get(i).getMove6());
            System.out.println("Move 7: " + listaPokemons.get(i).getMove7());
            System.out.println();
            System.out.println("-----------------");
            System.out.println();
        }
    }

    public void pesquisarPokemon() throws IOException {

        ReadCSV.reader(listaPokemons);
        boolean encontrado = false;
        String nome;
        Scanner scan = new Scanner(System.in);
        System.out.println("Digite o nome do pokemon para pesquisa: ");
        nome = scan.nextLine();

        for(int i = 0; i < listaPokemons.size(); ++i) {
            if(listaPokemons.get(i).getNome().equalsIgnoreCase(nome)) {
                System.out.println("Name: " + listaPokemons.get(i).getNome());
                System.out.println("Type 1: " + listaPokemons.get(i).getType1());
                System.out.println("Type 2: " + listaPokemons.get(i).getType2());
                System.out.println("---------");
                encontrado = true;
                break;
            }
        }
        if(!encontrado) {
            System.out.println("Pokemon inexistente");
        }
    }

    void pesquisarPeloTipoPokemon() throws IOException {
        ReadCSV.reader(listaPokemons);
        boolean encontrado = false;
        String tipo;
        Scanner scan = new Scanner(System.in);
        System.out.println("Digite o tipo do Pokemon: ");
        tipo = scan.nextLine();

        for(int i = 0; i < listaPokemons.size(); ++i) {
            if(listaPokemons.get(i).getType1().equalsIgnoreCase(tipo)) {
                System.out.println("Nome do Pokemon: " + listaPokemons.get(i).getNome());
                System.out.println("Tipo 1 do Pokemon: " + listaPokemons.get(i).getType1());
                System.out.println("Tipo 2 do Pokemon: " + listaPokemons.get(i).getType2());
                System.out.println("-------------------");
                encontrado = true;
            }
        }
        if(!encontrado) {
            System.out.println("Tipo digitado inexistente");
        }
    }
}
