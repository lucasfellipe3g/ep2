package Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import Model.*;

public class Pokedex {

    public static ArrayList<Pokemon> pokemons = new ArrayList<>();

    public static void main(String[] args) {

        CadastrarTreinador cadastrar = new CadastrarTreinador();
        GerenciandoPokemons gerenciar = new GerenciandoPokemons();

        try {
            BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
            String text = "";
            char option = '1';

            while(true) {
                System.out.println("Seja bem-vindo a POKEDEX: ");
                System.out.println("[1] - Cadastrar Treinador: ");
                System.out.println("[2] - Listar treinadores: ");
                System.out.println("[3] - Remover Treinador: ");
                System.out.println("[4] - Listar Pokemons: ");
                System.out.println("[5] - Pesquisar Pokemon: ");
                System.out.println("[6] - Procurar Pokemon pelo tipo: ");
                System.out.println("-----------------------------------");
                System.out.printf("Sua opção: ");

                text = teclado.readLine();
                option = text.charAt(0);

                switch(option) {
                    case '1':
                        cadastrar.cadastrarTreinador();
                        break;
                    case '2':
                        cadastrar.listarTreinadores();
                        break;
                    case '3':
                        cadastrar.removerTreinador();
                        break;
                    case '4':
                        gerenciar.listarPokemons();
                        break;
                    case '5':
                        gerenciar.pesquisarPokemon();
                        break;
                    case '6':
                        gerenciar.pesquisarPeloTipoPokemon();
                        break;

                }
            }
        } catch(IOException e) {
            Logger.getLogger(Pokedex.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
