# Pokédex

![Pokedex Logo](src/view/images/pokedex.png)

Before everything, open the ep2 folder on the eclipse or any java IDE, go in the ReadCSV Class and change the file path according to your computer(Image risk):

![Caminho do Arquivo](src/view/images/path.png)

The abcde project made in **** Java **** aims to visualize Pokémon's and their respective characteristics.

### Project features

​	The Pokédex contains several functions, among which we have:

- Register a coach;
- Visualization of the Pokémon and its characteristics;
- Associate five pokémons to your coach at the time of registration;
- Search pokémons by name;
- Generate all Pokémon of a certain type.



### Execution of the program

#### Install JAVA

```sh

$ sudo add-apt-repository ppa:webupd8team/java

$ sudo apt-get update

$ sudo apt-get install oracle-java8-installer

$ sudo apt-get install openjdk-8-jdk

```

#### Download the components of my repository by following the steps:

> git clone **https://gitlab.com/lucasfcm9/ep2.git**

#### To download the Pokédex.jar file directly through the link, click on the link below:

> **https://gitlab.com/lucasfcm9/ep2/raw/master/Pokedex.jar?inline=false**

Logo depois de realizar os passos acima, abra a pasta ep2 e execute o arquivo Pokedex.jar ou abra o terminal no mesmo diretório onde foi baixado o arquivo e execute:

> `java -jar Pokedex.jar`

#### Running my code through the terminal or the IDE:

Open my project in some IDE(Eclipse, IntelliJ Idea, netBeans) and click src to show the files inside the folder, done that, go to the Package Controller and click the file Pokedex.java and run it in the IDE itself.

![Executando Pokedex](src/view/images/mainPokedex.png)

Run the main. Soon after running the code, it will generate 6 options:

- [1] - Cadastrar Treinador
- [2] - Listar Treinadores
- [3] - Remover Treinador
- [4] - Listar Pokemons
- [5] - Pesquisar Pokemon
- [6] - Procurar Pokemon pelo tipo
![Pokedex2](src/view/images/pokedex2.png)
Choose one of these options and follow the steps.

#### Interface

When running the file Pokedex.jar, we will have this screen, where it will be possible to register a trainer, get a Pokémon by name and exit.

![Interface](src/view/images/interface.png)

To register a coach, fill in the name, Login, Password and his Pokémons fields from 1 to 5. When you finish, click on register and after that you will see an image of this writter "Treinador cadastrado com sucesso!".

![Interface2](src/view/images/interface2.png)

#### Search Pokémon

When you want to get a Pokémon, click on Find Pokémon and a window will appear asking you to enter the name of the Pokémon;

![Interface3](src/view/images/interface3.png)

When u enter the Pokémon name, all its attributes will appear, as in the example below. Charmander was typed and the following attributes appeared:

![Atributos do Pokemon](src/view/images/interface4.png)

When the registration of the coach, we can view each register, remove a coach, seek coach and list all coaches. To do this, click "Opçōes";

![Opções](src/view/images/interface5.png)

To list coaches, click "Opçōes", and the click "Listar treinadores";
![Listar Treinadores](src/view/images/listar.png)

To find the trainer, click on "Buscar treinador" and type his name, same in the register and the report with the name, login, password and all his Pokémon will appear;

![Relatorio](src/view/images/relatorio.png)

If you want to remove a trainer, enter the index of the array in which it was registered, remembering that it starts at 0;

![Remover Treinador](src/view/images/remover.png)

If u want to list the number of trainers in the Pokédex by clicking on "Quantidade de treinadores";

![Quantidade de treinadores](src/view/images/qnt.png)

And finally, we can list all the pokémons of a certain type by clicking "Procurar por tipo":

— The only problem is that opens window by window to list all types, however, it lists one by one with the name and type of the Pokémon:

![Listar por tipo](src/view/images/tipo1.png)

![Listar por tipo2](src/view/images/tipo2.png)

